package com.example.persons

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.android.synthetic.main.users_layout.*
import java.text.FieldPosition
import java.util.*
import kotlin.collections.ArrayList

const val REQUEST_CODE = 20

class UsersActivity : AppCompatActivity() {
    private var users: MutableList<UserModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        init()
    }


    private fun init() {
        saveChangesButton.setOnClickListener {
            getEditedParameters()
        }

            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = RecyclerViewAdapter(users, object: OnClick{
                override fun onCLick(position: Int) {
                    removeUser(position)
                }
            })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val changedInformation = data?.extras?.getParcelable<UserModel>("UserInformation")
            nameTextView.text = changedInformation!!.name
            surnameTextView.text = changedInformation.surname
            emailTextView.text = changedInformation.email
        }

    }

    private fun getEditedParameters() {
        val intent = Intent(this, EditActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }


    private fun removeUser(position: Int){
        users.removeAt(position)
        recyclerView.adapter!!.notifyItemRemoved(0)

    }


}
