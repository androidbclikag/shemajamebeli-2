package com.example.persons


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.users_layout.view.*
import kotlinx.android.synthetic.main.users_layout.view.emailTextView
import kotlinx.android.synthetic.main.users_layout.view.nameTextView
import kotlinx.android.synthetic.main.users_layout.view.surnameTextView


class RecyclerViewAdapter(private val myDataSet:MutableList<UserModel>, private val editUser:OnClick): RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    inner class MyViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView), View.OnClickListener {
        fun onBind() {
            val user = myDataSet[adapterPosition]
        itemView.nameTextView.text = user.name
            itemView.surnameTextView.text = user.surname
            itemView.emailTextView.text = user.email
            itemView.imageButton.setImageResource(user.image)
        }

        override fun onClick(v: View?) {
            editUser.onCLick(adapterPosition)
        }


    }

    override fun getItemCount(): Int = myDataSet.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.users_layout, parent, false))


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()

    }


}