package com.example.persons

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit.*

class EditActivity : AppCompatActivity() {
    private lateinit var changeUser:UserModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        init()
    }

    private fun init(){
            saveChangesButton.setOnClickListener {
                savedChanges()
            }
    }


    private fun savedChanges(){
        val name =nameEditText.text.toString()
        val surname = surnameEditText.text.toString()
        val email = emailEditText.text.toString()
        changeUser = UserModel(name, surname, email)
        val intent = intent.putExtra("UserInformation", changeUser)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
